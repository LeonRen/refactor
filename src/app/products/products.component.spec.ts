import { TestBed, async, ComponentFixture } from '@angular/core/testing';

import { ProductsComponent } from './products.component';
import { ProductsService } from '../products.service';
import { currencies } from '../currency.constant';

describe('ProductsComponent', () => {
  
  let fixture: ComponentFixture<ProductsComponent>;
  let component: ProductsComponent;

  beforeEach(() => {
      
      TestBed.configureTestingModule({
      declarations: [
        ProductsComponent
      ],
    });
  });

  beforeEach(() => {

  	fixture = TestBed.createComponent(ProductsComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create the app', () => {

      const app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
  });

  it('should use array of products from the service', () => {
  	  
  	  const service = fixture.debugElement.injector.get(ProductsService);
  	  fixture.detectChanges();
  	  expect(service.getProductsCurrency(currencies)).toEqual(component.productsCurrency);

  }
  	);

});
 