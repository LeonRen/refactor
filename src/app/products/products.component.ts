import { Component, OnInit } from '@angular/core';

import { ProductsService } from '../products.service';
import { currencies } from '../currency.constant';
import { Products } from '../products.model';

@Component({
	selector: 'app-products',
	templateUrl: './products.component.html',
	styleUrls: ['products.component.css']
})
export class ProductsComponent implements OnInit {
    
	productsCurrency: Array<Products>;

    constructor( private productsService: ProductsService ) {

        this.productsCurrency = productsService.getProductsCurrency(currencies);
	}

	ngOnInit() {

	}

	trackByIndex(index, item) {
		return index;
	}


}