import { TestBed, inject } from '@angular/core/testing';

import { ProductsService } from './products.service';
import { CurrencyService } from './currency.service';
import { currencies } from './currency.constant';

describe('ProductsService', () => {
    
    let proService: ProductsService;
    let curService: CurrencyService;

    beforeEach(() => {
        
        curService = new CurrencyService();
    	proService = new ProductsService(curService);

    });

     it("should contain an instance of currency service", () => {

    	expect(proService.currencyServiceInstance).toBeTruthy();
    });

    it("should get an array with the length of currencies key number", () => {

    	expect(proService.getProductsCurrency(currencies).length).toEqual(Object.keys(currencies).length);
    });

    it("should get an instance of Products with two keys", () => {

    	expect(Object.keys(proService.getProducts(currencies.nzd.symbol)).length).toEqual(2);

    });
});