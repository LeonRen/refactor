// Declare Object from external scripts
declare const LawnmowerRepository: any;
declare const PhoneCaseRepository: any;
declare const TShirtRepository: any;

export const productTypes = { lawnmower: 'Lawnmower', phoneCase: 'Phone Case', tShirt: 'T-Shirt' };

export const productsRepo = {
	lawnmower: {
	    repo: new LawnmowerRepository(),
	    type: productTypes.lawnmower,
	},
	phoneCase: {
		repo: new PhoneCaseRepository(),
	    type: productTypes.phoneCase,
	},
	tShirt: {
	    repo: new TShirtRepository(),
	    type: productTypes.tShirt,
	},
};

