export const currencies = { 
	nzd: {
	    symbol:'NZD',
	    rate: 1,
	},
    usd: {
        symbol: 'USD',
        rate: 0.76,
    },
    euro: {
        symbol: 'Euro',
        rate: 0.67,
    },
};