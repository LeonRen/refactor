export class Product {
	id: number;
	name: string;
	price: string;
	type: string;
} 

export class Products {
        productList: Product[];
    	currency: string;
}

