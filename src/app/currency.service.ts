import { Injectable } from '@angular/core';

import { currencies } from './currency.constant';
// Provide a root Dependency Injection service 
@Injectable({
    providedIn: 'root',
}) 
export class CurrencyService{
    
    public currency: string;

    constructor(){

    }
    
    setCurrency(currency: string): void {
        this.currency = currency;
    }
    
    //Convert number using certain currency rate and format
    currencyConverter(price: number): string {
        
        const keys = Object.keys( currencies );

        for(const key of keys)
        {
            if( currencies[key].symbol === this.currency )
            {
            	const priceFormatted = price* currencies[key].rate;
            	return priceFormatted.toFixed(2);
            }
        } 
    }
}