import { Injectable } from '@angular/core';

import { productTypes, productsRepo } from './products.constant';
import { currencies } from './currency.constant';
import { CurrencyService } from './currency.service';
import { Product, Products } from './products.model';


// Provide a root Dependency Injection service 
@Injectable({
	providedIn: 'root',
})
export class ProductsService {
    
    currencyServiceInstance: CurrencyService;
	
	constructor(private currencyService: CurrencyService) {

       this.currencyServiceInstance = currencyService;
    }

	//Get Products for all currency type
	getProductsCurrency(currencies: object): Products[] {
        
        const keys = Object.keys(currencies)
		return keys.map(key=>this.getProducts(currencies[key].symbol));
	}
    
    //Get Products in a certain currency
	getProducts(currency: string): Products {
        
        this.currencyServiceInstance.setCurrency(currency);

        const products: Products = {
			productList: (() => {
				    
				    let productList: Product[] = [];
                    
                    const typeKeys = Object.keys(productsRepo);

				    for(const typeKey of typeKeys)
				    {    
				    	const tempProducts = productsRepo[typeKey].repo.getAll();

                        const formatProducts = tempProducts.map( tempProduct => ({
                            id: tempProduct.id,
                            name: tempProduct.name,
                            price: this.currencyServiceInstance.currencyConverter(tempProduct.price),
                            type: productsRepo[typeKey].type,
                        }) );

                        formatProducts.map( formatProduct => { productList.push(formatProduct); });
                    }
				    return productList;
			})(),
			currency: currency,
		};

		return products;
	}
}