import { TestBed, inject } from '@angular/core/testing';

import { CurrencyService } from './currency.service';
import { currencies } from './currency.constant';

describe('CurrencyService', () => {
    
    let service: CurrencyService;

    beforeEach(() => {
        
        service = new CurrencyService();

    });

     it("should assign a value to currency", () => {

        service.setCurrency(currencies.nzd.symbol);
        expect(service.currency).toEqual(currencies.nzd.symbol);
    });

    it("should convert price with correct rate and format", () => {
        
        service.setCurrency(currencies.usd.symbol);
        const rate = currencies.usd.rate
        const price = 3000;
    	expect(service.currencyConverter(price)).toEqual((price*rate).toFixed(2));
    });

});